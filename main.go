package main

import (
	//	"errors"
	"fmt"
	"log/slog"
	"os"

	"gitlab.com/abitrolly/dnf-go-gui/parser"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

// Define some constants for readability
const Fullscreen = true
const Resize = true
const Visible = true
const Hidden = false

func setShortcuts(app *tview.Application, rootpager *tview.Pages, infobox *tview.Modal) {
	// Child widgets can not override parent keys :(
	// https://github.com/rivo/tview/issues/715

	// Global app shortcurts, which can not be overridden by widgets.
	// Default `tview` behavior it to quit on Ctrl+C.
	// Nothing to add here.
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		return event
	})

	// Root widget shortcuts, not overridable by child widgets.
	rootpager.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// Key is a code
		if event.Key() == tcell.KeyESC {
			slog.Info("ESC pressed in root widget")
			app.Stop()
		}
		// Key is a character
		if event.Key() == tcell.KeyRune {
			if event.Rune() == 'q' {
				app.Stop()
			}
			if event.Rune() == 'i' {
				rootpager.ShowPage("infobox")
			}
			if event.Rune() == 'o' {
				rootpager.SwitchToPage("list")
			}
		}
		return event
	})

	// Set event handling
	infobox.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyESC {
			slog.Info("ESC pressed")
			//pager.HidePage("infobox")
			//return nil
		}
		return nil //event
	})
}

func main() {
	// Read `dnf` data
	slog.Info("Parsing `dnf` output ...")

	var updates []parser.PackageUpdate
	updates = parser.UpdatesFromRepoQuery()

	// Create UI elements
	// 1. list of updates
	list := tview.NewList()
	// Set Box properties on the List component
	list.SetBorder(true).SetTitle(" package updates ")
	// Set List properties
	list.SetWrapAround(false)
	list.ShowSecondaryText(false)

	for _, u := range updates {
		verstr := u.NewVer
		if u.NewVer != u.OldVer {
			verstr += " (" + u.OldVer + ")"
		}
		update := fmt.Sprintf("%-30s  %-20s %s", u.Name, verstr, u.Summary)
		list.AddItem(update, "", 0, nil)
	}

	// 2. modal with detailed info
	infobox := tview.NewModal().
		AddButtons([]string{"Quit", "Cancel"}).
		SetText("Lorem Ipsum")

	// 3. layout with two pages (second page is needed to show modal)
	pager := tview.NewPages().
		AddPage("list", list, Resize, Visible).
		AddPage("infobox", infobox, Resize, Hidden)

	// Attach UI elements tree to app and start it
	app := tview.NewApplication()
	setShortcuts(app, pager, infobox)
	app.SetRoot(pager, Fullscreen)
	if err := app.Run(); err != nil {
		slog.Error("abnormal app exit", "error", err)
		os.Exit(1)
	}
}
