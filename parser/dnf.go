package parser

import (
	"errors"
	"log/slog"
	"os"
	"os/exec"
	"strings"
)

type PackageUpdate struct {
	Name    string
	OldVer  string
	NewVer  string
	Summary string
}

// UpdatesFromRepoQuery runs and parses `dnf repoquery` command to get
// the list of upcoming updates
func UpdatesFromRepoQuery() []PackageUpdate {
	var result []PackageUpdate
	for _, line := range RepoQuery("--upgrades") {
		s := strings.SplitN(line, " ", 3)
		name, version, summary := s[0], s[1], s[2]
		result = append(result, PackageUpdate{name, "", version, summary})
	}
	installed := make(map[string]string)
	for _, line := range RepoQuery("--installed") {
		s := strings.SplitN(line, " ", 3)
		name, version := s[0], s[1]
		installed[name] = version
	}
	for i, update := range result {
		_, ok := installed[update.Name]
		if ok {
			result[i].OldVer = installed[update.Name]
		}
	}
	return result
}

// RepoQuery runs `dnf repoquery` with params and returns list of strings.
// querytype is usually --installed or --upgrades
func RepoQuery(querytype string) []string {
	queryformat := "%{name} %{version} %{summary}"
	cmd := exec.Command("dnf", "repoquery", "--quiet", "--cacheonly",
		querytype, "--queryformat", queryformat)
	out, err := cmd.Output()
	if err != nil {
		/*
			`dnf` success return values:

			* 0: success, no updates

			Everything else is an error.
			https://dnf.readthedocs.io/en/latest/command_ref.html
		*/

		// `dnf` not found
		//     2019/11/26 19:31:47 UpdateList: *exec.Error
		//     exit status 1
		// `dnf` returned non-0 exit code
		//     2019/11/26 19:33:42 UpdateList: *exec.ExitError
		//     exit status 1
		var exitError *exec.ExitError
		if errors.As(err, &exitError) {
			switch exitError.ExitCode() {
			case 0:
				slog.Info("No updates.")
			default:
				slog.Error("`dnf` returned error", "exitcode", exitError.ExitCode())
				os.Exit(1)
			}
		} else {
			slog.Error("`dnf` run failed", "error", err)
			os.Exit(1)
		}
	}
	outstr := strings.TrimSpace(string(out))
	if outstr == "" {
		return []string{}
	} else {
		return strings.Split(outstr, "\n")
	}
}
