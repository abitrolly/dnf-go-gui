provider "gitlab" {
}


resource "gitlab_project" "default" {
  name             = "dnf-go-gui"
  description      = "DNF What's New for upcoming package updates."
  visibility_level = "public"
}
