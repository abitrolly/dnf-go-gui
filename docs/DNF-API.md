The only API that `dnf` and `libdnf` provides for Go programs is CLI.
https://github.com/rpm-software-management/libdnf/issues/720#issuecomment-632130117

And there is no dedicated CLI API really - only standard `dnf` commands.
Although there is a proposal to [add CSV export](https://bugzilla.redhat.com/show_bug.cgi?id=1836520)
to `dnf list`.

### Getting the list of updates from `dnf`

Important points.

* there are multiple commands for handling updates and it still takes two
  command to get list of updates and their previous version
* `dnf` checks for updates automatically for each command - this can be
  avoided with `-C, --cacheonly` option
* `dnf` prints extra line about cache - using `-q, --quiet` removes it


```
dnf list upgrades -q
dnf check-update
dnf update
dnf check-update --changelogs
dnf repoquery --changelog <package>
```

##### Why no `dnf update`

The reasons why I need TUI for `dnf update` is that

* I am curous what each package does
* Want to see if package update is critical (CVE ..)
* Need changelogs, with all versions since installed
* Need upstream changelogs as well or URLs to them
* URLs for the heads up on the software

Problem with getting info from `dnf update`

* demands `sudo`
* provides no `--dry-run` option
* no option to get info and quit
* output is in non-machine readable format
* no info about previous package version


##### `list upgrades` vs `check-update`

`dnf list upgrades` shows no info about previous package versions.

![dnf list upgrades](dnf-list-upgrades.png)

It also adds `.fc32` suffix to version field and architecture to
package names. It is impossible to remove extra line even with `-q`.

![dnf list upgrades -q](dnf-list-upgrades-q.png)

`dnf check update` output is almost identical to `list upgrades`.

![dnf check-update](dnf-check-update.png)

Except that "Available Upgrades:" line is now empty and won't go away
with `-q` option. `check update` also shows packages that are going
to be obsoleted at the end of the output.

![dnf check update2](dnf-check-update2.png)

Neither `list upgrades` nor `check-update` show information about
new packages being installed as dependencies that `dnf update` shows.

![dnf update](dnf-update.png)


##### The ideal output format

The full information about the package update includes.

* name
* old, new version - either normal version without epoch, or "none"
* one line description - what the package is about
* changes - between installed and new version
* if update is critical - what are CVEs
* what packages are system installed (blue), what are user installed (yellow) and
  what are their automatic dependencies (white)

Some of the information is available through `dnf repoquery`. For
example `dnf repoquery --upgrades --queryformat "%{name} %{version}"`
shows name and version of updates, but not installed versions. The
full list of tags can be inspected with `--querytags` option.

```
name, arch, epoch, version, release, reponame (repoid), evr,
debug_name, source_name, source_debug_name,
installtime, buildtime, size, downloadsize, installsize,
provides, requires, obsoletes, conflicts, sourcerpm,
description, summary, license, url, reason
```

The query to get information about upcoming updates.

    "%{name}   %{version}   %{arch}%  %{release}  %{size}"

Then another query is needed to get old versions of packages that
are being updated.

`-C, --cacheonly` option can be used to speed up `dnf` call.

The hint about using `repoquery --qf` came from
https://github.com/rpm-software-management/libdnf/issues/720#issuecomment-631471791

