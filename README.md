TUI for listing `dnf` updates written in Go.

![screenshot](./docs/dnf-go-gui.png)

There are multiple ways to get information about available updates in DNF,
but no fast interface to read all the necessary info including. 

* name
* old, new version
* one line description
* changes
* if the update is critical
* what packages are system installed (blue), what are user installed (yellow) and
  what are their automatic dependencies (white)

Details notes are in [docs/](./docs/) directory.

##### TODO

* [ ] Return proper error from `UpdateList()` instead of terminating there
* [ ] Strip .fc32 suffixes, but show if they differ
* [ ] Easy comparison for version field - inline diff with color, or two lines diff
