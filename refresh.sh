#!/bin/bash

set -x

# Refresh dependencies
# https://github.com/golang/go/wiki/Modules#how-to-upgrade-and-downgrade-dependencies

echo "== Available dependency updates =="
go list -u -m all

echo
echo "== Getting the updates =="
go get -v -u ./...

echo "== Cleaning up old dependencies =="
go mod tidy

echo
echo "== Building gdnf =="
go build
